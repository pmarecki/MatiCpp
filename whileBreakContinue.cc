    int a = 1;
    int b = 1000000;
    int ir = 0;

    while(a * b!=0) {       //powtarzamy kod tak dlugo, jesli zgadza sie warunek
        while(b>=a) {
            b -= a;
            ir++;
        }
        swap(a, b);
    }
    cout << ir << endl;

    REP(i,20) {
        if (i%7==3) continue;       //nie idziemy dalej w kodzie, tylko nastepne `i`
        cout << i << endl;
        if (i%7==6) break;          //konczymy petle
    }


