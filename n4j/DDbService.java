public enum MLabel implements Label{
    U,
    G,
    P,
    E
}

public enum MRelType implements RelationshipType {
    TO
}

package service;

import model.MLabel;
import model.MRelType;
import org.neo4j.cypher.ExecutionEngine;
import org.neo4j.cypher.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.tracing.PageCacheTracer;
import org.neo4j.kernel.impl.cache.CacheProvider;
import org.neo4j.kernel.impl.cache.NoCacheProvider;
import org.neo4j.kernel.impl.cache.WeakCacheProvider;
import scala.collection.Iterator;

import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Neo4j transactions:
 * 	success()   :   Marks this transaction as successful, which means that it will be committed upon invocation of `close()` unless failure() has or will be invoked before then.
 *  failure()   :   Marks this transaction as failed, which means that it will unconditionally be rolled back when close() is called.
 * 	close()     :   Commits or marks this transaction for rollback, depending on whether success() or failure() has been previously invoked.
 *
 * 	acquireReadLock(PropertyContainer entity)
 * 	acquireWriteLock(PropertyContainer entity)
 */


//Parameters: http://neo4j.com/docs/developer-manual/current/#cypher-parameters
public class DDbService {
    @Context
    private GraphDatabaseService db;
    private Transaction tx;
    public String PROP = "PROP";
//    public String DB_NAME = "GU2";   //mid
    public String DB_NAME = "GU3";     //large

    //Offline storage
    int nUsers;
    int nGroups;
    int nEnts;

    //helpful only in code after creation of DB
    Node[] users_;
    Node[] groups_;
    Node[] ents_;


    private void setConstantsMid() {
        nUsers = (int)1e6;
        nGroups = (int)1e4;
        nEnts = (int)1e4;
        //users->(100)groups
    }
    private void setConstantsLarge() {
        nUsers = (int)2e6;
        nGroups = (int)1e4;
        nEnts = (int)1e4;
        //users->(200)groups
    }


    public DDbService() {
        GraphDatabaseFactory factory = new GraphDatabaseFactory();
//        factory.setCacheProviders(caches);
//        StrongCacheProvider
//        WeakCacheProvider
//        NoCacheProvider
        System.out.println(GraphDatabaseSettings.cache_type.getDefaultValue());
        db = factory.newEmbeddedDatabaseBuilder(DB_NAME)
                .setConfig(GraphDatabaseSettings.cache_type, NoCacheProvider.NAME)  //object cache
                .setConfig(GraphDatabaseSettings.internal_log_location, "neo.log")
//                .setConfig(GraphDatabaseSettings.pagecache_memory, "abra-kadabra")
                .setConfig(GraphDatabaseSettings.pagecache_memory, "1M") //1M, 11G
                .newGraphDatabase();

//        db = new GraphDatabaseFactory().newEmbeddedDatabase(DB_NAME);
    }

    public Node create(MLabel label, String val) {
        Node n = db.createNode(label);
        n.setProperty(PROP, val);
        return n;
    }


    public void queryNumU() {
        tx = db.beginTx();
        ExecutionEngine engine = new ExecutionEngine(db, null);
        ExecutionResult result = engine.execute("match (n) return count(n) as w");
        Iterator<Node> n_column = result.columnAs( "w" );
        while(n_column.hasNext()) {
            System.out.println(n_column.next());
        }
        tx.success();
    }


    public void queryNumU(String val) {
        tx = db.beginTx();
        ExecutionEngine engine = new ExecutionEngine(db, null);
        ExecutionResult result = engine.execute("match (n:U) return count(n) as w");
        Iterator<Node> n_column = result.columnAs( "w" );
        while(n_column.hasNext()) {
            System.out.println(n_column.next());
        }
        tx.success();
    }


    //ASSIGNMENTS (need tx)
    public void link_U_G(Node u, Node g) {
        u.createRelationshipTo(g, MRelType.TO);
    }
    public void link_U_P(Node u, Node p) {
        u.createRelationshipTo(p, MRelType.TO);
    }
    public void link_P_G(Node p, Node g) {
        p.createRelationshipTo(g, MRelType.TO);
    }
    public void link_G_E(Node g, Node e) {
        g.createRelationshipTo(e, MRelType.TO); }


    public List<Node> findAllU() {
        List<Node> res = new ArrayList<>();
        tx = db.beginTx();
        ResourceIterator<Node> it = db.findNodes(MLabel.U);
        while(it.hasNext()) {
            res.add(it.next());
        }
        tx.close();
        return res;
    }

    public List<Node> findAll() {
        List<Node> res = new ArrayList<>();
        beginTx();
        Iterator<Node> it = new ExecutionEngine(db,null).execute("match (n) return n").columnAs("n");
        while(it.hasNext()) {
            res.add(it.next());
        }
        tx.close();
        return res;
    }

    @Deprecated //slow
    public long countRelations() {
        ExecutionResult res = new ExecutionEngine(db, null)
                .execute("match ()-[r]-() return count(r) as x");
        return (Long)res.columnAs("x").next();
    }

    @Deprecated //slow
    public long countNodes() {
        ExecutionResult res = new ExecutionEngine(db, null)
                .execute("match (n) return count(n) as x");
        return (Long)res.columnAs("x").next();
    }

    public void deleteAll() {
        beginTx();
        for (int i = 0; i < 100000; i++) {
            //transaction splitting; loop ends by `break`
            ExecutionResult res = new ExecutionEngine(db, null)
                    .execute("match ()-[r]-() with r limit 100000 delete r return count(r) as x");
            Long ttt = (Long)res.columnAs("x").next();
            if (ttt==0) break;
            commitTx();
            beginTx();
            System.out.println(i + "]" + ttt);
        }
        separator();
        new ExecutionEngine(db, null).execute("match (n) delete n");    //this is small enough to go in one TX
        commitTx();
        System.out.println("delete completed");
    }

    public void deleteAllU() {
        beginTx();
        ExecutionResult res = new ExecutionEngine(db, null).execute("match (u:U) delete u return count(u) as rr");
        Iterator<Node> x = res.columnAs( "rr" );
        while(x.hasNext()) {
            System.out.println("nodes deleted: " + x.next());
        }
        commitTx();
    }

    public Node findOne(long id) {
        ExecutionResult res = new ExecutionEngine(db, null).execute("match (n) where ID(n)={mid} return n",
                Collections.singletonMap("mid", id));
        Iterator<Node> x = res.columnAs("n");
        if (!x.hasNext()) return null;
        return x.next();
    }

    /////////////////////////////////////////////////////////////////
    //Special queries
    List<Node> groupsOfUser(Node user) {
        beginTx();
        List<Node> ans = new ArrayList<>();
        ExecutionResult res = new ExecutionEngine(db, null).execute(
                "match (u:U)-[:TO]->gr where ID(u)={uid} return gr",
                Collections.singletonMap("uid", user.getId()));
        Iterator<Node> x = res.columnAs("gr");
        while(x.hasNext()) {
            ans.add(x.next());
        }
//        System.out.println("groups of user:" + user + ": ");
//        ans.forEach(this::printNode);
        separator();
        commitTx();
        return ans;
    }


    List<Node> entsOfUser(Node user) {
        beginTx();
        ExecutionResult res = new ExecutionEngine(db, null).execute(
                "match (u:U)-[:TO]->(g:G)-[:TO]->e where ID(u)={uid} return e as rrr",
                Collections.singletonMap("uid", user.getId()));
        List<Node> ans = unpackNodes(res);
        commitTx();
        return ans;
    }

    List<Node> groupsOfEnt(Node ent) {
        beginTx();
        ExecutionResult res = new ExecutionEngine(db, null).execute(
                "match (g:G)-[:TO]->(e:E) where ID(e)={eid} return DISTINCT g as rrr",
                Collections.singletonMap("eid", ent.getId()));
        List<Node> ans = unpackNodes(res);
        commitTx();
        return ans;
    }

    List<Node> usersOfGroup(Node gr) {
        beginTx();
        ExecutionResult res = new ExecutionEngine(db, null).execute(
                "match (u:U)-[:TO]->(g:G) where ID(g)={gid} return DISTINCT u as rrr",
                Collections.singletonMap("gid", gr.getId()));
        List<Node> ans = unpackNodes(res);
        commitTx();
        return ans;
    }

    List<Node> usersOfEnt(Node ent) {
        beginTx();
        System.out.println("prep");
        ExecutionResult res = new ExecutionEngine(db, null).execute(
                "match (u:U)-[:TO]->(g:G)-[:TO]->(e:E) where ID(e)={eid} return DISTINCT u as rrr",
                Collections.singletonMap("eid", ent.getId()));
        //list of results from this column (for "return u, g", res.columns()->{"u","g"})
        List<Node> ans = unpackNodes(res);
        commitTx();
        return ans;
    }

    public Node findByProperty(MLabel label, String val) {
        beginTx();
        Node n = db.findNode(label, PROP, val);
        commitTx();
        return n;
    }

    //needs column `rrr`
    List<Node> unpackNodes(ExecutionResult res) {
        List<Node> ans = new ArrayList<>();
        Iterator<Node> x = res.columnAs("rrr");
        while(x.hasNext()) {
            ans.add(x.next());
        }
        res.close();
        return ans;
    }

    /////////////////////////////////////////////////////////////////
    //Benchmarks
    void benchmarkUsers() {
        int n = 100;
        Random r = new Random();
        long st = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            int unum = r.nextInt(nUsers);
            findByProperty(MLabel.U, "User" + unum);
//            System.out.println(i);
        }
        System.out.println("Avg user find time: " + (System.currentTimeMillis()-st)/n);
    }

    void benchmarkGroups() {
        int n = 100;
        Random r = new Random();
        long st = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            int gnum = r.nextInt(nGroups);
            findByProperty(MLabel.G, "Group" + gnum);
        }
        System.out.println("Avg group find time: " + (System.currentTimeMillis()-st)/n);
    }

    void benchmarkEnts() {
        int n = 100;
        Random r = new Random();
        long st = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            int gnum = r.nextInt(nEnts);
            findByProperty(MLabel.E, "Ent" + gnum);
        }
        System.out.println("Avg ent find time: " + (System.currentTimeMillis()-st)/n);
    }

    void testEntsOfUser() {
        int n = 10;
        Random r = new Random();
        long st = System.currentTimeMillis();
        long sum = 0;
        for (int i = 0; i < n; i++) {
            int unum = r.nextInt(nUsers);
            sum += entsOfUser(findByProperty(MLabel.U, "User" + unum)).size();
        }
        System.out.println("Avg EoU query duration: " + (System.currentTimeMillis()-st)/n);
        System.out.println("avg count=" + sum/n);
    }

    void testUsersOfEnt() {
        int n = 10;
        Random r = new Random();
        long st = System.currentTimeMillis();
        long sum = 0;
        for (int i = 0; i < n; i++) {
            int unum = r.nextInt(nEnts);
            sum += usersOfEnt(findByProperty(MLabel.E, "Ent" + unum)).size();
        }
        System.out.println("Avg user find time: " + (System.currentTimeMillis()-st)/n);
        System.out.println("avg count=" + sum/n);
    }

    void testUsersOfGroup() {
        int n = 10;
        Random r = new Random();
        long st = System.currentTimeMillis();
        long sum = 0;
        for (int i = 0; i < n; i++) {
            int rnd = r.nextInt(nGroups);
            sum += usersOfGroup(findByProperty(MLabel.G, "Group" + rnd)).size();
            System.out.println(i);
        }
        System.out.println("Avg time: " + (System.currentTimeMillis()-st)/n + " avg count=" + (sum/n));
    }

    void testGroupsOfEnt() {
        int n = 10000;
        Random r = new Random();
        long st = System.currentTimeMillis();
        long sum = 0;
        for (int i = 0; i < n; i++) {
            int rnd = r.nextInt(nEnts);
            sum += groupsOfEnt(findByProperty(MLabel.E, "Ent" + rnd)).size();
            System.out.println(i);
        }
        System.out.println("Avg time: " + (System.currentTimeMillis()-st)/n + " avg count=" + (sum/n));
    }




    //benchmarkFindingEntsOfUser_Inverse

    void createIndexU() {
        beginTx();
        ExecutionResult res = new ExecutionEngine(db, null).execute("create index on :U(" + PROP + ")");
        commitTx();
        System.out.println("Index created");
    }
    void dropIndexU() {
        beginTx();
        ExecutionResult res = new ExecutionEngine(db, null).execute("drop index on :U(" + PROP + ")");
        commitTx();
        System.out.println("Index dropped");
    }
    void showIndexes() {
        beginTx();
        db.schema().getIndexes().forEach(in->{
            System.out.println(in);
        });
        commitTx();
    }


    ///////////////////////////////////////
    //Utils

    public void printNode(Node n) {
        beginTx();
        if (n==null) {
            System.out.println("<null>");
            return;
        }
        System.out.printf("%15s %10s %10s\n", n, n.getLabels(), n.getProperty(PROP,"--null--"));
        commitTx();
    }

    public void separator() {
        System.out.println("-----------------");
    }

    private void commitTx() {
        tx.success();
        tx.close();
    }

    public void beginTx() {
        tx = db.beginTx();
    }

    public void createExplicitDB() {
        nUsers = 5;
        nGroups = 3;
        nEnts = 5;

        deleteAll();
        users_ = new Node[nUsers];
        groups_ = new Node[nGroups];
        ents_ = new Node[nEnts];

        beginTx();
        for (int i = 0; i < nUsers; i++) {
            String name = "User" + i;
            users_[i] = create(MLabel.U, name);
        }
        for (int i = 0; i < nGroups; i++) {
            String name = "Group" + i;
            groups_[i] = create(MLabel.G, name);
        }
        for (int i = 0; i < nEnts; i++) {
            String name = "Ent" + i;
            ents_[i] = create(MLabel.E, name);
        }
        commitTx();
        //attachments
        beginTx();
        link_U_G(users_[0], groups_[0]);
        link_U_G(users_[0], groups_[2]);
        link_U_G(users_[4], groups_[1]);
        link_G_E(groups_[0], ents_[0]);
        link_G_E(groups_[1], ents_[0]);
        link_G_E(groups_[2], ents_[1]);
        commitTx();
        //check groups attached to user
        groupsOfUser(users_[0]);
        entsOfUser(users_[0]);
        usersOfEnt(ents_[0]);
    }

    //100 ents/group, `groupsPerUser` variable/settable
    public void createDatabase(int groupsPerUser) {
        deleteAll();

        int entPerGroup = 100;
        int txSize = 5000;

        users_ = new Node[nUsers];
        groups_ = new Node[nGroups];
        ents_ = new Node[nEnts];

        long st = System.currentTimeMillis();

        //Create nodes
        beginTx();
        for (int i = 0; i < nUsers; i++) {
            String name = "User" + i;
            users_[i] = create(MLabel.U, name);
            if (i%txSize==0) {
                commitTx();
                beginTx();
                System.out.println("User"+i+" created");
            }
        }
        commitTx();
        beginTx();
        System.out.println(nUsers + " users created");
        for (int i = 0; i < nGroups; i++) {
            String name = "Group" + i;
            groups_[i] = create(MLabel.G, name);
            if (i%txSize==0) {
                commitTx();
                beginTx();
            }
        }
        commitTx();
        beginTx();
        System.out.println(nGroups + " groups created");
        for (int i = 0; i < nEnts; i++) {
            String name = "Ent" + i;
            ents_[i] = create(MLabel.E, name);
            if (i%txSize==0) {
                commitTx();
                beginTx();
            }
        }
        commitTx();
        System.out.println(nEnts + " ents created");
        System.out.println("Creation duration: " + (System.currentTimeMillis() - st) + "[ms]");
        st = System.currentTimeMillis();

        //Create relations

        beginTx();
        Random r = new Random(111);
        //users->groups
        for (int i = 0; i < nUsers; i++) {
            Set<Integer> grs = new HashSet<>();
            while (grs.size()<groupsPerUser) grs.add(r.nextInt(nGroups));
            for(int gr : grs) {
                link_U_G(users_[i], groups_[gr]);
            }
            if (i%txSize==0) {
                commitTx();
                beginTx();
                System.out.println("User"+i+" linked");
            }
        }
        commitTx();
        beginTx();
        //users->groups
        for (int i = 0; i < nGroups; i++) {
            Set<Integer> ents = new HashSet<>();
            while (ents.size()<entPerGroup) ents.add(r.nextInt(nEnts));
            for(int en : ents) {
                link_G_E(groups_[i], ents_[en]);
            }
            if (i%txSize==0) {
                commitTx();
                beginTx();
                System.out.println("Group"+i+" linked");
            }
        }
        commitTx();
        System.out.println("Creation of relations duration: " + (System.currentTimeMillis() - st) + "[ms]");
        st = System.currentTimeMillis();
    }


    public void testIt() {
//        deleteAllU();
//        createG("13");
//        createExplicitDB();
//        deleteAll();
//        createDatabase();
//        findAll().forEach(this::printNode);
//        System.out.println("relations: " + countRelations());
//        System.out.println("nodes: " + countNodes());

        //Set user number etc:
//        setConstantsMid();
        setConstantsLarge();

        //Optional create DB (arg=groups per user)
//        createDatabase(200);

        //Index (search by property)
//        createIndexU();
//        dropIndexU();
//        showIndexes();

//        benchmarkUsers();
//        benchmarkGroups();
//        benchmarkEnts();
//        testEntsOfUser();
//        testUsersOfEnt();
//        testUsersOfGroup();
        testGroupsOfEnt();
//        testUsersOfGroup();
    }


}
