cmake_minimum_required(VERSION 3.2)
project(mati_cpp)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_executable(wykopki operacje_bity.cc)